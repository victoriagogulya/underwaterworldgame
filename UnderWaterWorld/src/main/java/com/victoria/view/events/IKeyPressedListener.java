package com.victoria.view.events;


@FunctionalInterface
public interface IKeyPressedListener {

    void update(int key, boolean keyPressed);
}
