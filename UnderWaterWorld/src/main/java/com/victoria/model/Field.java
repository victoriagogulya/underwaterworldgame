package com.victoria.model;


import com.victoria.model.units.Unit;

public class Field extends Unit {

    private static Field field = new Field();

    private Field() {
        coordinates.x = 0;
        coordinates.y = 0;
        width = 750;
        height = 530;
    }

    public static Field instance() { return field; }

    @Override
    public void move() {}
}
