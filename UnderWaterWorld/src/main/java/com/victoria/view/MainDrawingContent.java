package com.victoria.view;


import javax.swing.*;
import java.awt.*;

public class MainDrawingContent extends JComponent {

    private Image imageBack;


    public MainDrawingContent(int width, int height) {

        this.setSize(width, height);
        imageBack = new ImageIcon(getClass().getResource("/underwater.jpg")).getImage();
    }

    @Override
    protected void paintComponent(Graphics g) {
       g.drawImage(imageBack, 0, 0, getWidth(), getHeight(), null);
    }
}
