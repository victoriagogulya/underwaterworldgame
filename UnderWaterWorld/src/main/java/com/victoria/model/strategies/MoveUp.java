package com.victoria.model.strategies;


import com.victoria.model.units.Speed;

public class MoveUp extends MoveStrategy {
    @Override
    public void update(Speed speed) {
        speed.speedY = -speed.offset;
        if (playerSpeed != null && playerSpeed.speedX > 0)
            speed.speedX = -playerSpeed.speedX;
        else
            speed.speedX = 0;
    }
}
