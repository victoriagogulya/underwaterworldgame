package com.victoria.view.events;


import com.victoria.common.Buttons;

@FunctionalInterface
public interface IButtonPressedListener {

    void update(Buttons button);

}
