package com.victoria.strategies;


import com.victoria.model.strategies.MoveLeft;
import com.victoria.model.units.Speed;
import org.junit.Test;
import static org.junit.Assert.*;


public class MoveLeftTest {

    @Test
    public void updateTest() {
        Speed speed = new Speed();
        speed.offset = 10;
        MoveLeft strategy = new MoveLeft();

        int expectedSpeedX = -speed.offset;
        int expectedSpeedY = speed.speedY;
        strategy.update(speed);

        assertEquals(expectedSpeedX, speed.speedX);
        assertEquals(expectedSpeedY, speed.speedY);
    }
}
