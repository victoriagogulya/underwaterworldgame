package com.victoria.model.strategies;


import com.victoria.model.units.Speed;

public class NoMove extends MoveStrategy {

    @Override
    public void update(Speed speed) {
        if (playerSpeed != null && playerSpeed.speedX > 0)
            speed.speedX = -playerSpeed.speedX;
        else
            speed.speedX = 0;
    }
}
