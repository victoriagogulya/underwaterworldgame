package com.victoria.model.units;


public abstract class Unit {
    protected Coord coordinates = new Coord();
    protected int height;
    protected int width;
    protected Speed speed = new Speed();


    public Coord getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Coord coordinates) {
        this.coordinates = coordinates;
    }

    public int getX() {
        return coordinates.x;
    }

    public int getY() {
        return coordinates.y;
    }

    public void setX(int x) {
        coordinates.x = x;
    }

    public void setY(int y) {
        coordinates.y = y;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() { return width; }

    public void setWidth(int width) {
        this.width = width;
    }

    public Speed getSpeed() { return speed; }

    public void setSpeed(Speed speed) { this.speed = speed; }

    public abstract void move();
}
