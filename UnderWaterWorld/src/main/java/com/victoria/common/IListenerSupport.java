package com.victoria.common;

public interface IListenerSupport<T> {

    void addListener(T listener);

    void removeListener(T listener);
}
