package com.victoria.controller.events;


public interface IWorkIsDoneListener {

    void update();
}
