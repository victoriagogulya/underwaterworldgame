package com.victoria.model.scores;


import java.io.Serializable;

public class Score implements Comparable<Score>, Serializable {

    private final String name;
    private final int value;

    public Score(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public int compareTo(Score o)  {
        if(o == null) throw new NullPointerException();
        if(this == o || value == o.value) return 0;
        if(value > o.value) return 1;
        else return -1;
    }
}
