package com.victoria.model.factories;


import com.victoria.common.IFactorySupport;
import com.victoria.model.strategies.*;
import com.victoria.model.units.Participant;
import com.victoria.model.units.Speed;
import com.victoria.model.units.VisualType;

public class ParticipantFactory implements IFactorySupport<Participant> {

    private static ParticipantFactory inst = new ParticipantFactory();

    private ParticipantFactory() { }

    public static ParticipantFactory instance() { return inst; }

    @Override
    public Participant create(VisualType type) {

        Participant tmp = null;
        switch(type) {
            case GreyFish:
                tmp =  new Participant(new MoveRight());
                tmp.setSpeed(new Speed(15));
                tmp.setWidth(130);
                tmp.setHeight(80);
                tmp.setCollisionable(true);
                break;
            case Grass20:
                tmp =  new Participant(new NoMove());
                tmp.setSpeed(new Speed(0));
                tmp.setWidth(40);
                tmp.setHeight(70);
                tmp.setCollisionable(false);
                break;
            case Grass21:
                tmp =  new Participant(new NoMove());
                tmp.setSpeed(new Speed(0));
                tmp.setWidth(30);
                tmp.setHeight(150);
                tmp.setCollisionable(false);
                break;
            case Coral22:
                tmp =  new Participant(new NoMove());
                tmp.setSpeed(new Speed(0));
                tmp.setWidth(70);
                tmp.setHeight(70);
                tmp.setCollisionable(false);
                break;
            case Grass23:
                tmp =  new Participant(new NoMove());
                tmp.setSpeed(new Speed(0));
                tmp.setWidth(70);
                tmp.setHeight(80);
                tmp.setCollisionable(false);
                break;
            case BubbleSmall:
                tmp =  new Participant(new MoveUp());
                tmp.setSpeed(new Speed(5));
                tmp.setWidth(30);
                tmp.setHeight(30);
                tmp.setCollisionable(false);
                break;
            case StarRed:
                tmp =  new Participant(new NoMove());
                tmp.setSpeed(new Speed(0));
                tmp.setWidth(40);
                tmp.setHeight(40);
                tmp.setCollisionable(false);
                break;
            case AngelFish:
                tmp =  new Participant(new MoveRight());
                tmp.setSpeed(new Speed(10));
                tmp.setWidth(80);
                tmp.setHeight(80);
                tmp.setCollisionable(true);
                break;
            case BlueFish:
                tmp =  new Participant(new MoveLeft());
                tmp.setSpeed(new Speed(5));
                tmp.setWidth(90);
                tmp.setHeight(80);
                tmp.setCollisionable(true);
                break;
            case Crab:
                tmp =  new Participant(new MoveLeftRight());
                tmp.setSpeed(new Speed(6));
                tmp.setWidth(70);
                tmp.setHeight(70);
                tmp.setCollisionable(true);
                break;
            case Dolphin:
                tmp =  new Participant(new MoveLeftWave());
                tmp.setSpeed(new Speed(6));
                tmp.setWidth(100);
                tmp.setHeight(80);
                tmp.setCollisionable(true);
                break;
            case TreasureChest:
                tmp =  new Participant(new NoMove());
                tmp.setSpeed(new Speed(0));
                tmp.setWidth(200);
                tmp.setHeight(200);
                tmp.setCollisionable(true);
                break;
        }
        return tmp;
    }
}
