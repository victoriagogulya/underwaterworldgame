package com.victoria.model;


import com.victoria.common.IRunnable;
import com.victoria.common.Keys;

public interface IGameModel extends IRunnable{
    void initialization();
    void updatePlayer(Keys direction, boolean keyPressed);
}
