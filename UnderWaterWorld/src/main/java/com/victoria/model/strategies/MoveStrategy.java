package com.victoria.model.strategies;


import com.victoria.model.events.IPlayerMovedListener;
import com.victoria.model.events.PlayerMovedEvent;
import com.victoria.model.units.Speed;

public abstract class MoveStrategy {

    protected static Speed playerSpeed;

    public abstract void update(Speed speed);

    static {
        IPlayerMovedListener listener = playerSpeed -> MoveStrategy.playerSpeed = playerSpeed;
        PlayerMovedEvent.instance().addListener(listener);
    }
}
