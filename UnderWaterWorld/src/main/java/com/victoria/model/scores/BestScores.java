package com.victoria.model.scores;


import java.io.Serializable;
import java.util.*;

public class BestScores implements IBestScores, Serializable {

    private final List<Score> list = new ArrayList<>();
    public static final int MAX_COUNT = 10;

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Iterator<Score> iterator() {
        return new Iterator<Score>() {
            private Iterator<Score> iterator = list.iterator();
            @Override
            public boolean hasNext() {
                return iterator.hasNext();
            }

            @Override
            public Score next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }
                return iterator.next();
            }
        };
    }

    @Override
    public boolean canBeAdded(Score score) {

        List<Score> tempScores = new ArrayList<>();
        tempScores.add(score);
        Collections.sort(list);
        return !(tempScores.size() == MAX_COUNT + 1 && tempScores.get(MAX_COUNT) == score);
    }

    public void add(Score score) {
        if (canBeAdded(score))
        {
            list.add(score);
            Collections.sort(list);
            if (list.size() == MAX_COUNT + 1)
                list.remove(MAX_COUNT);
        }
    }
}
