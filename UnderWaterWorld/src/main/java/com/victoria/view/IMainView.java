package com.victoria.view;


import com.victoria.view.events.ButtonPressedEvent;

public interface IMainView extends IVisibility{

    ButtonPressedEvent.MainButtonPressedEvent getButtonPressedEvent();
}
