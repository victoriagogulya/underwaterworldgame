package com.victoria.model.units;


import com.victoria.model.strategies.MoveStrategy;

public class Food extends Participant {

    private int value;

    public Food(MoveStrategy moveStrategy, int value) {
        super(moveStrategy);
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
