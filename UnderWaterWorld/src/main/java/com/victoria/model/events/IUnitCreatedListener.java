package com.victoria.model.events;


import com.victoria.model.units.Unit;
import com.victoria.model.units.UnitType;
import com.victoria.model.units.VisualType;

@FunctionalInterface
public interface IUnitCreatedListener {

    void update(UnitType unitType, VisualType visualType, Unit unit);
}
