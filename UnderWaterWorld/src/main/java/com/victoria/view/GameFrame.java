package com.victoria.view;


import com.victoria.common.Buttons;
import com.victoria.model.events.GameModelChangedEvent;
import com.victoria.view.events.ButtonPressedEvent;
import com.victoria.view.events.KeyPressedEvent;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;


public class GameFrame extends JFrame implements IGameView {

    private GameDrawingContent content = new GameDrawingContent();
    private JPanel panel = new JPanel();
    private JLabel labelScore;
    private JLabel labelLives;


    public GameFrame() {
        setSize(750, 630);
        setResizable(false);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Underwater world");
        initialization();

        GameModelChangedEvent.instance().addListener(state -> {
                labelScore.setText(String.valueOf(state.getScore()));
                labelLives.setText(String.valueOf(state.getPlayer().getLives()));
        });

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                generateExitEvent();
            }
        });
    }

    @Override
    public ButtonPressedEvent.GameButtonPressedEvent getButtonPressedEvent() {
        return ButtonPressedEvent.GAME_FRAME;
    }

    @Override
    public KeyPressedEvent getKeyPressedEvent() {
        return KeyPressedEvent.instance();
    }

    @Override
    public void visibility(boolean value) {
        setVisible(value);
        if (!value)
            dispose();
    }

    @Override
    public void initialization() {
        content.initialization();
        labelScore = new JLabel();
        labelLives = new JLabel();
        createGUI();
    }

    private void createGUI() {
        panel.setLayout(null);
        panel.setFocusable(true);
        content.setBounds(0, 0, 750, 530);
        panel.add(content);

        Font font = new Font("Arial", Font.BOLD, 16);
        createLabels(font);
        add(panel);
        panel.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {

                KeyPressedEvent.instance().OnKeyPressed(e.getKeyCode(), true);
            }

            @Override
            public void keyReleased(KeyEvent e) {
                KeyPressedEvent.instance().OnKeyPressed(e.getKeyCode(), false);
            }
        });
    }

    private void createLabels(final Font font) {
        JLabel label1 = new JLabel("Score:");
        label1.setBounds(30, 550, 60, 30);
        label1.setFont(font);
        label1.setForeground(Color.BLUE);
        label1.setOpaque(true);
        panel.add(label1);

        labelScore.setBounds(90, 550, 70, 30);
        labelScore.setFont(font);
        labelScore.setHorizontalAlignment(JLabel.LEFT);
        labelScore.setForeground(Color.BLUE);
        labelScore.setOpaque(true);
        panel.add(labelScore);

        JLabel label2 = new JLabel("Lives:");
        label2.setBounds(190, 550, 60, 30);
        label2.setFont(font);
        label2.setForeground(Color.BLUE);
        label2.setOpaque(true);
        panel.add(label2);

        labelLives.setBounds(280, 550, 70, 30);
        labelLives.setFont(font);
        labelLives.setHorizontalAlignment(JLabel.LEFT);
        labelLives.setForeground(Color.BLUE);
        labelLives.setOpaque(true);
        panel.add(labelLives);
    }

    private void generateExitEvent() {
        ButtonPressedEvent.GAME_FRAME.OnButtonPressed(Buttons.Exit);
    }
}


