package com.victoria.model.units;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;


@RunWith(Parameterized.class)
public class PlayerTest {

    private Player player;
    private Coord coord;

    public PlayerTest(Coord coord) {
        this.coord = coord;
    }

    @Parameterized.Parameters
    public static Collection<Object> coordinates() {
        return Arrays.asList(new Object[] {
                new Coord(10, 10),
                new Coord(300, 10),
        });
    }

    @Before
    public void create() {
        player = new Player();
        player.setCoordinates(coord);
    }

    @After
    public void dispose() {
        player = null;
    }

    @Test
    public void moveUpTest() {
        int expectedX = player.getX();
        int expectedY = player.getY() - player.getSpeed().offset;

        player.getSpeed().speedY = -player.getSpeed().offset;
        player.move();

        assertEquals(expectedX, player.getX());
        assertEquals(expectedY, player.getY());
    }

    @Test
    public void moveDownTest() {
        int expectedX = player.getX();
        int expectedY = player.getY() + player.getSpeed().offset;

        player.getSpeed().speedY = player.getSpeed().offset;
        player.move();

        assertEquals(expectedX, player.getX());
        assertEquals(expectedY, player.getY());
    }

    @Test
    public void moveLeftTest() {
        int expectedX = player.getX() - player.getSpeed().offset;
        int expectedY = player.getY();

        player.getSpeed().speedX = -player.getSpeed().offset;
        player.move();

        assertEquals(expectedX, player.getX());
        assertEquals(expectedY, player.getY());
    }

    @Test
    public void moveRightTest() {
        int expectedX = player.getX() + player.getSpeed().offset;
        if(player.getX() >= 300)
            expectedX = player.getX();
        int expectedY = player.getY();

        player.getSpeed().speedX = player.getSpeed().offset;
        player.move();

        assertEquals(expectedX, player.getX());
        assertEquals(expectedY, player.getY());
    }
}
