package com.victoria.model;

import com.victoria.common.Keys;
import com.victoria.model.collisions.CollisionDetection;
import com.victoria.model.events.GameModelChangedEvent;
import com.victoria.model.events.PlayerMovedEvent;
import com.victoria.model.events.UnitCreatedEvent;
import com.victoria.model.events.UnitRemovedEvent;
import com.victoria.model.factories.ParticipantFactory;
import com.victoria.model.generator.ParticipantsGenerator;
import com.victoria.model.units.*;
import com.victoria.utils.Randomize;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


public class GameModel implements IGameModel {
    private GameModelState gameState;
    private ParticipantsGenerator generator;
    private Player player;
    private List<Participant> participants;
    private List<Food> foods;
    private Field field;
    private Timer timer;

    public GameModel() {
        field = Field.instance();
        timer = new Timer(80, e -> gameIteration());
    }

    @Override
    public void run() {
        timer.start();
        generator.run();
    }

    @Override
    public void stop() {
        timer.stop();
        generator.stop();
    }

    @Override
    public void updatePlayer(Keys direction, boolean keyPressed) {
        switch (direction) {
            case UP:
                if (keyPressed)
                    player.getSpeed().speedY = -player.getSpeed().offset;
                else
                    player.getSpeed().speedY = 0;
                break;
            case DOWN:
                if (keyPressed)
                    player.getSpeed().speedY = player.getSpeed().offset;
                else
                    player.getSpeed().speedY = 0;
                break;
            case RIGHT:
                if (keyPressed)
                    player.getSpeed().speedX = player.getSpeed().offset;
                else
                    player.getSpeed().speedX = 0;
                break;
            case LEFT:
                if (keyPressed)
                    player.getSpeed().speedX = -player.getSpeed().offset;
                else
                    player.getSpeed().speedX = 0;
                break;

        }
        PlayerMovedEvent.instance().OnPlayerMoved(player.getSpeed());
    }

    @Override
    public void initialization() {
        gameState = new GameModelState();
        generator = new ParticipantsGenerator();
        player = new Player();
        participants = new LinkedList<>();
        foods = new LinkedList<>();
        UnitCreatedEvent.instance().OnUnitCreated(UnitType.Field, VisualType.SeaBack1, field);
        UnitCreatedEvent.instance().OnUnitCreated(UnitType.Player, VisualType.YellowFish, player);
        Participant p = ParticipantFactory.instance().create(VisualType.StarRed);
        p.setY(field.getHeight() - p.getHeight() - Randomize.uniform(33, 71));
        p.setX(field.getWidth() - p.getWidth());
        UnitCreatedEvent.instance().OnUnitCreated(UnitType.Participant, VisualType.StarRed, p);
        participants.add(p);
        gameState.setField(field);
        gameState.setPlayer(player);
        gameState.setParticipants(participants);
        gameState.setFoods(foods);
    }

   private void gameIteration() {
       generationOfParticipants();
       generationOfFood();
       move();
       collisionOfParticipants();
       collisionOfFood();
       GameModelChangedEvent.instance().OnGameChanged(gameState);
   }

    private void collisionOfParticipants() {
        Iterator<Participant> iterator = participants.iterator();
        int counter = 0;
        List<Integer> list = new ArrayList<>();
        while(iterator.hasNext()) {
            Participant partic = iterator.next();
            if (CollisionDetection.isOutOfTheField(partic)) {
                list.add(counter);
                iterator.remove();
            }
            else if (partic.isCollisionable() && CollisionDetection.isIntersects(player, partic)) {
                player.setLives(player.getLives() - 1);
                partic.setCollisionable(false);
            }
            counter++;
        }
        if (!list.isEmpty())
            UnitRemovedEvent.instance().OnUnitRemoved(UnitType.Participant, list);
    }

    private void collisionOfFood() {
        if (!foods.isEmpty()) {
            Iterator<Food> iterator = foods.iterator();
            int counter = 0;
            List<Integer> list = new ArrayList<>();
            while(iterator.hasNext()) {
                Food food = iterator.next();
                if (CollisionDetection.isOutOfTheField(food)) {
                    list.add(counter);
                    iterator.remove();
                }
                else if (CollisionDetection.isIntersects(player, food)) {
                    gameState.setScore(gameState.getScore() + food.getValue());
                    list.add(counter);
                    iterator.remove();
                }
                counter++;
            }
            if (!list.isEmpty())
                UnitRemovedEvent.instance().OnUnitRemoved(UnitType.Food, list);
        }
    }

    private void generationOfParticipants() {
        if (generator.isGenerated()) {
            Participant participant = generator.getParticipant();
            if (participant.getSpeed().offset == 0 && player.getSpeed().speedX <= 0) {
                generator.setTaken(true);
                return;
            }
            participants.add(participant);
            UnitCreatedEvent.instance()
                    .OnUnitCreated(UnitType.Participant, generator.getVisualType(), participant);
            generator.setTaken(true);
        }

    }

    private void generationOfFood() {
        if (generator.isGeneratedFood()) {
            Food food = generator.getFood();
            if (food.getSpeed().offset == 0 && player.getSpeed().speedX <= 0) {
                generator.setTakenFood(true);
                return;
            }
            foods.add(food);
            UnitCreatedEvent.instance()
                    .OnUnitCreated(UnitType.Food, generator.getVisualTypeOfFood(), food);
            generator.setTakenFood(true);
        }
    }

    private void move() {
        player.move();
        participants.forEach(Participant::move);
        Participant p = participants.get(0);
        if (p.getX() < 0) {
            p.setY(field.getHeight() - p.getHeight() - Randomize.uniform(33, 71));
            p.setX(field.getWidth() - p.getWidth());
        }
        foods.forEach(Participant::move);
    }
}
