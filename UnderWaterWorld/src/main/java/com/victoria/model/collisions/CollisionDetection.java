package com.victoria.model.collisions;


import com.victoria.model.Field;
import com.victoria.model.units.Unit;

public class CollisionDetection {

    private static Field field = Field.instance();

    public static boolean isIntersects(final Unit unit1, final Unit unit2) {
        return unit1.getX() + unit1.getWidth() - 1 >= unit2.getX()
                && unit1.getY() + unit1.getHeight() - 1 >= unit2.getY()
                && unit2.getX() + unit2.getWidth() - 1 >= unit1.getX()
                && unit2.getY() + unit2.getHeight() - 1 >= unit1.getY();
    }

    public static boolean isOutOfTheField(final Unit unit) {
        return !(unit.getX() + unit.getWidth() - 1 >= field.getX()
                && unit.getY() + unit.getHeight() - 1 >= field.getY()
                && field.getX() + field.getWidth() - 1 >= unit.getX()
                && field.getY() + field.getHeight() - 1 >= unit.getY());
    }
}
