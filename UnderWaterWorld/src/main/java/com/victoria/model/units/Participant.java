package com.victoria.model.units;


import com.victoria.model.strategies.MoveStrategy;

public class Participant extends Unit {

    protected boolean collisionable;
    protected MoveStrategy moveStrategy;

    public Participant(MoveStrategy moveStrategy) { this.moveStrategy = moveStrategy; }

    public boolean isCollisionable() { return collisionable; }

    public void setCollisionable(boolean enemy) { this.collisionable = enemy; }

    @Override
    public void move() {
        moveStrategy.update(speed);
        coordinates.x += speed.speedX;
        coordinates.y += speed.speedY;
    }
}
