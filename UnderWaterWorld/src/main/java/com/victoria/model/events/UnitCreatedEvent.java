package com.victoria.model.events;

import com.victoria.common.IListenerSupport;
import com.victoria.model.units.Unit;
import com.victoria.model.units.UnitType;
import com.victoria.model.units.VisualType;

import java.util.ArrayList;


public class UnitCreatedEvent implements IListenerSupport<IUnitCreatedListener> {

    private static final UnitCreatedEvent inst = new UnitCreatedEvent();
    private ArrayList<IUnitCreatedListener> listeners = new ArrayList<>();

    private UnitCreatedEvent(){}

    public static UnitCreatedEvent instance() {
        return inst;
    }

    public void OnUnitCreated(UnitType unitType, VisualType visualType, Unit unit) {
        if (!listeners.isEmpty()) {
            for (IUnitCreatedListener tmp : listeners) {
                tmp.update(unitType, visualType, unit);
            }
        }
    }

    @Override
    public void addListener(IUnitCreatedListener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeListener(IUnitCreatedListener listener) {
        listeners.remove(listener);
    }
}
