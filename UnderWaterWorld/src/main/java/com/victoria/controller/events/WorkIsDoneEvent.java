package com.victoria.controller.events;


import com.victoria.common.IListenerSupport;

import java.util.LinkedList;
import java.util.List;

public class WorkIsDoneEvent implements IListenerSupport<IWorkIsDoneListener> {

    private List<IWorkIsDoneListener> list = new LinkedList<>();

    public void OnDone() {
        if (!list.isEmpty()) {
            list.forEach(IWorkIsDoneListener::update);
        }
    }
    @Override
    public void addListener(IWorkIsDoneListener listener) {
        list.add(listener);
    }

    @Override
    public void removeListener(IWorkIsDoneListener listener) {
        list.remove(listener);
    }
}
