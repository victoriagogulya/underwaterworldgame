package com.victoria.view;


import com.victoria.common.IView;

public interface IVisibility extends IView{

    void visibility(boolean value);
}
