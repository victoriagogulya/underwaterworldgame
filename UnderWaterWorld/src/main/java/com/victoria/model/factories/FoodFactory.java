package com.victoria.model.factories;


import com.victoria.common.IFactorySupport;
import com.victoria.model.strategies.MoveUpDown;
import com.victoria.model.units.Food;
import com.victoria.model.units.Speed;
import com.victoria.model.units.VisualType;

public class FoodFactory implements IFactorySupport<Food> {

    private static FoodFactory inst = new FoodFactory();

    private FoodFactory() {}

    public static FoodFactory instance() { return inst; }

    @Override
    public Food create(VisualType type) {
        Food food = null;

        switch(type) {
            case Beetle10:
                food =  new Food(new MoveUpDown(10), 10);
                food.setSpeed(new Speed(3));
                food.setWidth(30);
                food.setHeight(40);
                food.setCollisionable(true);
                break;
        }
        return food;
    }
}
