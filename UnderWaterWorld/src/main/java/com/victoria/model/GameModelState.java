package com.victoria.model;


import com.victoria.model.units.Food;
import com.victoria.model.units.Participant;
import com.victoria.model.units.Player;

import java.util.List;


public final class GameModelState {

    private Player player;
    private List<Participant> participants;
    private List<Food> foods;
    private Field field;
    private int score;

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public List<Participant> getParticipants() {
        return participants;
    }

    public void setParticipants(List<Participant> participants) {
        this.participants = participants;
    }

    public Field getField() {
        return field;
    }

    public void setField(Field field) {
        this.field = field;
    }

    public List<Food> getFoods() {
        return foods;
    }

    public void setFoods(List<Food> foods) {
        this.foods = foods;
    }
}
