package com.victoria.model.strategies;


import com.victoria.model.units.Speed;

public class MoveLeftWave extends MoveStrategy {

    private static final int MAX = 20;
    private int step;
    private boolean directionUp = true;

    @Override
    public void update(Speed speed) {
        if (playerSpeed != null && playerSpeed.speedX > 0)
            speed.speedX = -(speed.offset + playerSpeed.speedX);
        else
            speed.speedX = -speed.offset;
        if (directionUp)
            speed.speedY = speed.offset;
        else
            speed.speedY = -speed.offset;
        step++;
        if (step == MAX) {
            step = 0;
            directionUp = !directionUp;
        }
    }
}
