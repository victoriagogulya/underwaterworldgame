package com.victoria.model.events;


import com.victoria.model.units.UnitType;

import java.util.List;

@FunctionalInterface
public interface IUnitRemovedListener {

    void update(UnitType unitType, List<Integer> numberOfUnits);
}
