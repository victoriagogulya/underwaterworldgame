package com.victoria.view;


import javax.swing.*;
import java.awt.*;

public class TransparentDrawingContent extends JComponent {

    private Image imageBack;
    private Image imageFish;
    private Image imageCrab;
    private Image imageGrass;


    public TransparentDrawingContent(Dimension dimension) {

        this.setSize(dimension.width, dimension.height);
        imageBack = new ImageIcon(getClass().getResource("/underwater.jpg")).getImage();
        imageFish = new ImageIcon(getClass().getResource("/yellowFish.png")).getImage();
        imageCrab = new ImageIcon(getClass().getResource("/crab.png")).getImage();
        imageGrass = new ImageIcon(getClass().getResource("/grass20.png")).getImage();
    }

    @Override
    protected void paintComponent(Graphics g) {

        g.drawImage(imageBack, 0, 0, getWidth(), getHeight(), null);
        g.drawImage(imageFish, getWidth() / 2 - 50, getHeight() / 2, 70, 70, null);
        g.drawImage(imageCrab, getX() + 60, getHeight() - 100, 60, 60, null);
        g.drawImage(imageGrass, getX() + getWidth() - 100, getY() + getHeight() - 100, 60, 70, null);
    }
}
