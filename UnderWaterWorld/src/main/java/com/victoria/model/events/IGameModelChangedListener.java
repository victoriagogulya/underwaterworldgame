package com.victoria.model.events;


import com.victoria.model.GameModelState;

@FunctionalInterface
public interface IGameModelChangedListener {

    void update(GameModelState state);
}
