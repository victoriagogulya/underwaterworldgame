package com.victoria.model.scores;


public interface IBestScores extends Iterable<Score> {

    int getCount();
    boolean canBeAdded(Score score);
    void add(Score score);
}
