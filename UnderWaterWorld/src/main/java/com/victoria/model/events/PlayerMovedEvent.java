package com.victoria.model.events;


import com.victoria.common.IListenerSupport;
import com.victoria.model.units.Speed;

import java.util.ArrayList;

public class PlayerMovedEvent implements IListenerSupport<IPlayerMovedListener>{

    private static final PlayerMovedEvent inst = new PlayerMovedEvent();
    private ArrayList<IPlayerMovedListener> listeners = new ArrayList<>();

    private PlayerMovedEvent(){}

    public static PlayerMovedEvent instance() {
        return inst;
    }

    public void OnPlayerMoved(Speed playerSpeed) {
        if (!listeners.isEmpty()) {
            for (IPlayerMovedListener tmp : listeners) {
                tmp.update(playerSpeed);
            }
        }
    }

    @Override
    public void addListener(IPlayerMovedListener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeListener(IPlayerMovedListener listener) {
        listeners.remove(listener);
    }
}
