package com.victoria.view.events;

import com.victoria.common.IListenerSupport;
import java.util.ArrayList;

public class KeyPressedEvent implements IListenerSupport<IKeyPressedListener> {

    private static final KeyPressedEvent inst = new KeyPressedEvent();
    private ArrayList<IKeyPressedListener> listeners = new ArrayList<>();

    private KeyPressedEvent() {}

    public static KeyPressedEvent instance() {
        return inst;
    }

    public void OnKeyPressed(int key, boolean keyPressed) {
        if (!listeners.isEmpty()) {
            for (IKeyPressedListener tmp : listeners) {
                tmp.update(key, keyPressed);
            }
        }
    }

    @Override
    public void addListener(IKeyPressedListener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeListener(IKeyPressedListener listener) {
        listeners.remove(listener);
    }
}
