package com.victoria.strategies;

import com.victoria.model.strategies.MoveUp;
import com.victoria.model.units.Speed;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class MoveUpTest {

    @Test
    public void updateTest() {
        Speed speed = new Speed();
        speed.offset = 10;
        MoveUp strategy = new MoveUp();

        int expectedSpeedX = speed.speedX;
        int expectedSpeedY = -speed.offset;
        strategy.update(speed);

        assertEquals(expectedSpeedX, speed.speedX);
        assertEquals(expectedSpeedY, speed.speedY);
    }
}
