package com.victoria.view;

import com.victoria.model.events.*;
import com.victoria.model.units.*;

import javax.swing.*;
import java.awt.*;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


public class GameDrawingContent extends JComponent{

    private Image imagePlayer;
    private Rectangle rectPlayer;
    private Image imageField;
    private Rectangle rectField;
    private List<Image> imageList;
    private List<Rectangle> rectangleList;
    private Image imageFood;
    private List<Rectangle> rectFood;

    public GameDrawingContent()
    {
        GameModelChangedEvent.instance().addListener(state ->
            EventQueue.invokeLater(() -> {
                rectPlayer.setLocation(state.getPlayer().getX(), state.getPlayer().getY());
                Iterator<Rectangle> rectangleIterator = rectangleList.iterator();
                for (Participant part : state.getParticipants()) {
                    rectangleIterator.next().setLocation(part.getX(), part.getY());
                }
                Iterator<Rectangle> foodIterator = rectFood.iterator();
                for (Food food : state.getFoods()) {
                    foodIterator.next().setLocation(food.getX(), food.getY());
                }
                GameDrawingContent.this.repaint();
            }));

        UnitCreatedEvent.instance().addListener((unitType, visualType, unit) -> {
                try {
                    switch (unitType) {
                        case Player:
                           createPlayer(unit, visualType);
                            break;
                        case Field:
                           createField(unit, visualType);
                            break;
                        case Participant:
                            createParticipant(unit, visualType);
                            break;
                        case Food:
                           createFood(unit, visualType);
                            break;
                    }
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
        });

        UnitRemovedEvent.instance().addListener((unitType, numberOfUnits) -> {
                switch(unitType) {
                    case Participant:
                        removeParticipants(numberOfUnits);
                        break;
                    case Food:
                        removeFood(numberOfUnits);
                        break;
                }
        });
    }

    public void initialization() {
        imageList = new LinkedList<>();
        rectangleList = new LinkedList<>();
        rectFood = new LinkedList<>();
    }

    @Override
    public void paintComponent(Graphics g) {
        drawField(g);
        drawPlayer(g);
        drawParticipants(g);
        drawFood(g);
    }

    private void removeParticipants(List<Integer> integerList) {
        Iterator<Integer> integerIterator = integerList.iterator();
        Iterator<Rectangle> rectIterator = rectangleList.iterator();
        Iterator<Image> imageIterator = imageList.iterator();
        int counter = 0;
        int number = integerIterator.next();
        while(rectIterator.hasNext()) {
            rectIterator.next();
            imageIterator.next();
            if (counter == number) {
                rectIterator.remove();
                imageIterator.remove();
                if (integerIterator.hasNext())
                    number = integerIterator.next();
                else
                    break;
            }
            counter++;
        }
    }

    private void removeFood(List<Integer> integerList) {
        Iterator<Integer> integerIterator = integerList.iterator();
        Iterator<Rectangle> rectIterator = rectFood.iterator();

        int counter = 0;
        int number = integerIterator.next();
        while(rectIterator.hasNext()) {
            rectIterator.next();
            if (counter == number) {
                rectIterator.remove();
                if (integerIterator.hasNext())
                    number = integerIterator.next();
                else
                    break;
            }
            counter++;
        }
    }

    private void createPlayer(Unit unit, VisualType visualType) {
        imagePlayer = new ImageIcon(getClass().getResource(chooseImage(visualType))).getImage();
        rectPlayer = new Rectangle(unit.getX(), unit.getY(), unit.getWidth(), unit.getHeight());
    }

    private void createField(Unit unit, VisualType visualType) {
        imageField = new ImageIcon(getClass().getResource(chooseImage(visualType))).getImage();
        rectField = new Rectangle(unit.getX(), unit.getY(), unit.getWidth(), unit.getHeight());
    }

    private void createParticipant(Unit unit, VisualType visualType) {
        imageList.add(new ImageIcon(getClass().getResource(chooseImage(visualType))).getImage());
        rectangleList.add(new Rectangle(unit.getX(), unit.getY(), unit.getWidth(), unit.getHeight()));
    }

    private void createFood(Unit unit, VisualType visualType) {
        imageFood = new ImageIcon(getClass().getResource(chooseImage(visualType))).getImage();
        rectFood.add(new Rectangle(unit.getX(), unit.getY(), unit.getWidth(), unit.getHeight()));
    }

    private void drawField(Graphics g) {
        if (imageField != null) {
           g.drawImage(imageField, rectField.x, rectField.y, rectField.width, rectField.height, null);
        }
    }

    private void drawPlayer(Graphics g) {
        if (imagePlayer != null) {
            g.drawImage(imagePlayer, rectPlayer.x, rectPlayer.y, rectPlayer.width, rectPlayer.height, null);
        }
    }

    private void drawParticipants(Graphics g) {
        if (!imageList.isEmpty()) {
            Iterator<Image> imageIterator = imageList.iterator();
            Iterator<Rectangle> rectangleIterator = rectangleList.iterator();
            Rectangle rect = null;
            while (imageIterator.hasNext()) {
                rect = rectangleIterator.next();
                g.drawImage(imageIterator.next(), rect.x, rect.y, rect.width, rect.height, null);
            }
        }
    }

    private void drawFood(Graphics g) {
        if (!rectFood.isEmpty()) {
            for (Rectangle food : rectFood) {
                g.drawImage(imageFood, food.x, food.y, food.width, food.height, null);
            }
        }
    }

    private String chooseImage(VisualType type) {
        String str = null;
        switch (type) {
            case YellowFish:
                str = "/yellowFish.png";
                break;
            case SeaBack1:
                str = "/back1.png";
                break;
            case GreyFish:
                str = "/greyFish.png";
                break;
            case Grass20:
                str = "/grass20.png";
                break;
            case Grass21:
                str = "/grass21.png";
                break;
            case Coral22:
                str = "/coral22.png";
                break;
            case Grass23:
                str = "/grass23.png";
                break;
            case BubbleSmall:
                str = "/bubbleSmall.png";
                break;
            case StarRed:
                str = "/starRed.png";
                break;
            case AngelFish:
                str = "/angelFish.png";
                break;
            case BlueFish:
                str = "/blueFish.png";
                break;
            case Crab:
                str = "/crab.png";
                break;
            case Dolphin:
                str = "/dolphin.png";
                break;
            case TreasureChest:
                str = "/treasureChest.png";
                break;
            case Beetle10:
                str = "/beetle10.png";
                break;
        }
        return str;
    }
}
