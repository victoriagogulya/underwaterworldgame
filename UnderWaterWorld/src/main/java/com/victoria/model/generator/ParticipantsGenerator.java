package com.victoria.model.generator;


import com.victoria.model.Field;
import com.victoria.model.factories.FoodFactory;
import com.victoria.model.factories.ParticipantFactory;
import com.victoria.model.units.Food;
import com.victoria.model.units.Participant;
import com.victoria.model.units.VisualType;
import com.victoria.utils.Randomize;

import javax.swing.*;

public class ParticipantsGenerator {

    protected Field field;
    protected Participant newParticipant;
    protected VisualType visualType;
    protected Food food;
    protected VisualType visualTypeFood;
    protected Timer timer;

    public ParticipantsGenerator() {
        field = Field.instance();
        timer = new Timer(1500, e -> {
                generate();
                generateFood();
        });
    }

    public boolean isGenerated() { return newParticipant != null; }

    public Participant getParticipant() { return newParticipant; }

    public VisualType getVisualType() { return visualType; }

    public void setTakenFood(boolean taken) {
        if (taken) {
            food = null;
            visualTypeFood = null;
        }
    }

    public boolean isGeneratedFood() { return food != null; }

    public Food getFood() { return food; }

    public VisualType getVisualTypeOfFood() { return visualTypeFood; }

    public void setTaken(boolean taken) {
        if (taken) {
            newParticipant = null;
            visualType = null;
        }
    }

    public void run() { timer.start(); }

    public void stop() { timer.stop(); }

    protected void generate() {
        int number = Randomize.uniform(0, 51);
        if (number < 10)
            generateCollisionableParticipants();
        else if (number >= 10 && number < 35)
            generateNotCollisionableParticipants();
        else
            newParticipant = null;
    }

    protected void generateFood() {
        if (Randomize.uniform(100, 250) < 110) {
            visualTypeFood = VisualType.Beetle10;
            food = FoodFactory.instance().create(visualTypeFood);
            food.setY(Randomize.uniform(50, field.getHeight() - 100));
            food.setX(field.getWidth());
        }
    }

    private void generateCollisionableParticipants() {
        int type = Randomize.uniform(2, 8);
        switch(type) {
            case 2:
                visualType = VisualType.GreyFish;
                newParticipant = ParticipantFactory.instance().create(visualType);
                newParticipant.setY(Randomize.uniform(field.getHeight() - 100));
                newParticipant.setX(-10);
                break;
            case 3:
                visualType = VisualType.AngelFish;
                newParticipant = ParticipantFactory.instance().create(visualType);
                newParticipant.setY(Randomize.uniform(field.getHeight() - 100));
                newParticipant.setX(-10);
                break;
            case 4:
                visualType = VisualType.BlueFish;
                newParticipant = ParticipantFactory.instance().create(visualType);
                newParticipant.setY(Randomize.uniform(field.getHeight() - 100));
                newParticipant.setX(field.getWidth());
                break;
            case 5:
                visualType = VisualType.Crab;
                newParticipant = ParticipantFactory.instance().create(visualType);
                newParticipant.setY(field.getHeight() - newParticipant.getHeight() - Randomize.uniform(33, 71));
                newParticipant.setX(field.getWidth());
                break;
            case 6:
                visualType = VisualType.Dolphin;
                newParticipant = ParticipantFactory.instance().create(visualType);
                newParticipant.setY(Randomize.uniform(field.getHeight() - 200));
                newParticipant.setX(field.getWidth());
                break;
            case 7:
                visualType = VisualType.TreasureChest;
                newParticipant = ParticipantFactory.instance().create(visualType);
                newParticipant.setY(field.getHeight() - newParticipant.getHeight() - 20);
                newParticipant.setX(field.getWidth());
                break;
        }
    }

    private void generateNotCollisionableParticipants() {
        int type = Randomize.uniform(20, 25);
        switch(type) {
            case 20:
                visualType = VisualType.Grass20;
                newParticipant = ParticipantFactory.instance().create(visualType);
                newParticipant.setY(field.getHeight() - newParticipant.getHeight() - Randomize.uniform(33, 71));
                break;
            case 21:
                visualType = VisualType.Grass21;
                newParticipant = ParticipantFactory.instance().create(visualType);
                newParticipant.setY(field.getHeight() - newParticipant.getHeight() - Randomize.uniform(33, 71));
                break;
            case 22:
                visualType = VisualType.Coral22;
                newParticipant = ParticipantFactory.instance().create(visualType);
                newParticipant.setY(field.getHeight() - newParticipant.getHeight() - Randomize.uniform(33, 71));
                break;
            case 23:
                visualType = VisualType.Grass23;
                newParticipant = ParticipantFactory.instance().create(visualType);
                newParticipant.setY(-10);
                break;
            case 24:
                visualType = VisualType.BubbleSmall;
                newParticipant = ParticipantFactory.instance().create(visualType);
                newParticipant.setY(field.getHeight() - newParticipant.getHeight() - Randomize.uniform(33, 71));
                break;
        }
        newParticipant.setX(field.getWidth() - newParticipant.getWidth());
    }
}
