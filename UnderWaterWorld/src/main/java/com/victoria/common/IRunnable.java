package com.victoria.common;


public interface IRunnable {
    void run();
    void stop();
}
