package com.victoria.model.scores;


import java.io.*;

public class BestScoresStorage implements IBestScoresStorage{

    private final String fileName;

    public BestScoresStorage(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public  void save(IBestScores bestScores) throws IOException {

        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName));
        oos.writeObject(bestScores);
        oos.flush();
        oos.close();
    }

    @Override
    public IBestScores load() throws IOException, ClassNotFoundException {

        if (!new File(fileName).exists()) throw new IOException();
        ObjectInputStream oin = new ObjectInputStream(new FileInputStream(fileName));
        return (BestScores)oin.readObject();
    }
}
