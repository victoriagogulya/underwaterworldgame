package com.victoria.program;


import com.victoria.controller.GameController;
import com.victoria.controller.IController;
import com.victoria.controller.MainController;
import com.victoria.model.GameModel;
import com.victoria.view.GameFrame;
import com.victoria.view.MainFrame;

import java.awt.*;


public class Main {
    public static void main(String[] args) {

        final Dimension dimension= new Dimension(500, 400);

        final GameController gameController = new GameController(new GameFrame(), new GameModel());
        final IController controller = new MainController(new MainFrame(dimension), gameController);

        controller.run();
    }
}
