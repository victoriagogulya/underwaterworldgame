package com.victoria.model.units;


public enum VisualType {
    YellowFish(0),
    SeaBack1(1),
    GreyFish(2),
    AngelFish(3),
    BlueFish(4),
    Crab(5),
    Dolphin(6),
    TreasureChest(7),
    Beetle10(10),
    Grass20(20),
    Grass21(21),
    Coral22(22),
    Grass23(23),
    BubbleSmall(24),
    StarRed(50);

    private int value;

    VisualType(int value) { this.value = value; }

    public int getValue() { return value; }
}
