package com.victoria.view;

import com.victoria.common.Buttons;
import com.victoria.view.events.ButtonPressedEvent;

import javax.swing.*;
import java.awt.*;

public class MainFrame extends JFrame implements IMainView {

    private MainDrawingContent content;
    private JPanel panel = new JPanel();
    private JButton buttonPlay = new JButton();

    public MainFrame(Dimension dimension) {
        setSize(dimension.width, dimension.height);
        content = new MainDrawingContent(dimension.width, dimension.height - 10);
        setResizable(false);
        setLocationRelativeTo(null);
        setTitle("Underwater World");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        createGUI();
        new TransparentFrame(dimension);
    }

    @Override
    public ButtonPressedEvent.MainButtonPressedEvent getButtonPressedEvent() {
        return ButtonPressedEvent.MAIN_FRAME;
    }

    @Override
    public void visibility(boolean value) {
        setVisible(value);
    }

    private void createGUI() {
        panel.setLayout(null);
        panel.setFocusable(true);
        createButtons();
        panel.add(content);
        add(panel);
    }

    private void createButtons() {
        buttonPlay.setText("Play");
        buttonPlay.setBounds(180, 150, 80, 20);
        buttonPlay.addActionListener(e -> ButtonPressedEvent.MAIN_FRAME.OnButtonPressed(Buttons.Play));
        panel.add(buttonPlay);
    }
}

