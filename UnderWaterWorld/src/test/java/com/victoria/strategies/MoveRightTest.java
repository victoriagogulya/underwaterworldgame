package com.victoria.strategies;

import com.victoria.model.strategies.MoveRight;
import com.victoria.model.units.Speed;
import org.junit.Test;
import static org.junit.Assert.*;


public class MoveRightTest {

    @Test
    public void updateTest() {
        Speed speed = new Speed();
        speed.offset = 10;
        MoveRight strategy = new MoveRight();

        int expectedSpeedX = speed.offset;
        int expectedSpeedY = speed.speedY;
        strategy.update(speed);

        assertEquals(expectedSpeedX, speed.speedX);
        assertEquals(expectedSpeedY, speed.speedY);
    }
}
