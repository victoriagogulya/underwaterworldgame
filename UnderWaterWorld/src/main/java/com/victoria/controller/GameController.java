package com.victoria.controller;

import com.victoria.common.Keys;
import com.victoria.controller.events.WorkIsDoneEvent;
import com.victoria.model.IGameModel;
import com.victoria.view.IGameView;


import java.awt.*;
import java.awt.event.KeyEvent;


public class GameController implements IController {

    private  IGameView view;
    private IGameModel model;
    private final WorkIsDoneEvent workIsDoneEvent = new WorkIsDoneEvent();
    private ResultOfWork resultOfWork;

    public enum ResultOfWork { GameOver, Exit }

    public GameController(IGameView view, IGameModel model) {
        this.view = view;
        this.model = model;
        view.getKeyPressedEvent().addListener((key, keyPressed) -> {
            switch (key) {
                case KeyEvent.VK_LEFT:
                    model.updatePlayer(Keys.LEFT, keyPressed);
                    break;
                case KeyEvent.VK_RIGHT:
                    model.updatePlayer(Keys.RIGHT, keyPressed);
                    break;
                case KeyEvent.VK_UP:
                    model.updatePlayer(Keys.UP, keyPressed);
                    break;
                case KeyEvent.VK_DOWN:
                    model.updatePlayer(Keys.DOWN, keyPressed);
                    break;
            }
        });

        view.getButtonPressedEvent().addListener(button -> {
            switch(button) {
                case Exit:
                    model.stop();
                    resultOfWork = ResultOfWork.Exit;
                    workIsDoneEvent.OnDone();
                    break;
            }
        });
    }

    public void initialization() {
        view.initialization();
        model.initialization();
    }

    public WorkIsDoneEvent getWorkIsDoneEvent() {
        return workIsDoneEvent;
    }

    public ResultOfWork getResultOfWork() {
        return resultOfWork;
    }

    @Override
    public void run() {
        EventQueue.invokeLater(() -> view.visibility(true));
        model.run();
    }

    @Override
    public void stop() {
        view.visibility(false);
    }

}
