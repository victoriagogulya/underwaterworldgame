package com.victoria.model.units;


public class Player extends Unit{

    private int lives = 10;

    public Player() {
        speed.offset = 7;
        width = 80;
        height = 80;
        coordinates.x = 290;
        coordinates.y = 200;
    }

    public int getLives() {
        return lives;
    }

    public void setLives(int lives) {
        this.lives = lives;
    }

    @Override
    public void move() {
        if (speed.speedX < 0 || coordinates.x < 300)
            coordinates.x += speed.speedX;
        coordinates.y += speed.speedY;
    }
}
