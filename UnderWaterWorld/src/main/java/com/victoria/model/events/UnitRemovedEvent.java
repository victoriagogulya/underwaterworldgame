package com.victoria.model.events;


import com.victoria.common.IListenerSupport;
import com.victoria.model.units.UnitType;
import java.util.ArrayList;
import java.util.List;

public class UnitRemovedEvent implements IListenerSupport<IUnitRemovedListener> {

    private static final UnitRemovedEvent inst = new UnitRemovedEvent();
    private ArrayList<IUnitRemovedListener> listeners = new ArrayList<>();

    private UnitRemovedEvent() { }

    public static UnitRemovedEvent instance() {
        return inst;
    }


    public void OnUnitRemoved(UnitType unitType, List<Integer> units) {
        if (!listeners.isEmpty()) {
            for (IUnitRemovedListener tmp : listeners) {
                tmp.update(unitType, units);
            }
        }
    }

    @Override
    public void addListener(IUnitRemovedListener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeListener(IUnitRemovedListener listener) {
        listeners.remove(listener);
    }
}
