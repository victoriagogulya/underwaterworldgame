package com.victoria.model.strategies;


import com.victoria.model.units.Speed;

public class MoveUpDown extends MoveStrategy {

    private int steps;
    private int stepsMax;
    private boolean directionUp = true;

    public MoveUpDown(int stepMax) {
        this.stepsMax = stepMax;
    }

    @Override
    public void update(Speed speed) {
        if (playerSpeed != null && playerSpeed.speedX > 0)
            speed.speedX = -playerSpeed.speedX;
        else
            speed.speedX = 0;
        if (directionUp)
            speed.speedY = -speed.offset;
        else
            speed.speedY = speed.offset;

        steps++;
        if (steps == stepsMax) {
            steps = 0;
            directionUp = !directionUp;
        }
    }
}
