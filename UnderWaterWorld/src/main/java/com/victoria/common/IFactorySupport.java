package com.victoria.common;


import com.victoria.model.units.VisualType;

@FunctionalInterface
public interface IFactorySupport<T> {

    T create(VisualType type);
}
