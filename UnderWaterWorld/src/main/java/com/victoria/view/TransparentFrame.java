package com.victoria.view;


import javax.swing.*;
import java.awt.*;


public class TransparentFrame extends JFrame {

    private TransparentDrawingContent content;
    private JPanel panel = new JPanel();

    public TransparentFrame(Dimension dimension) {
        setSize(dimension.width, dimension.height);
        setResizable(false);
        setLocationRelativeTo(null);
        setTitle("");
        panel.setLayout(null);
        panel.setFocusable(true);
        content = new TransparentDrawingContent(dimension);
        panel.add(content);
        add(panel);
        setUndecorated(true);
        try {
            work();
        }catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
    }

    private void work() throws InterruptedException {
        setOpacity(0.0F);
        setVisible(true);
        for (int i = 0; i < 16; i++) {
            setOpacity(getOpacity() + 0.05F);
            Thread.sleep(150);
        }
        Thread.sleep(1000);
        setVisible(false);
    }
}
