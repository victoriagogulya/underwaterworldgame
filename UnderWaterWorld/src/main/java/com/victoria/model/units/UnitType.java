package com.victoria.model.units;


public enum UnitType {
    Player,
    Participant,
    Field,
    Food
}
