package com.victoria.view.events;


import com.victoria.common.Buttons;
import com.victoria.common.IListenerSupport;

import java.util.ArrayList;
import java.util.List;

public class ButtonPressedEvent {

    private static final ButtonPressedEvent inst = new ButtonPressedEvent();
    public static final MainButtonPressedEvent MAIN_FRAME = inst.new MainButtonPressedEvent();
    public static final GameButtonPressedEvent GAME_FRAME = inst.new GameButtonPressedEvent();

    private ButtonPressedEvent() {}

     public class MainButtonPressedEvent implements IListenerSupport<IButtonPressedListener> {

        private List<IButtonPressedListener> list = new ArrayList<>();
        private MainButtonPressedEvent() {}

        public void OnButtonPressed(final Buttons button) {
            if(!list.isEmpty()) {
                for(IButtonPressedListener tmp : list)
                    tmp.update(button);
            }
        }

        @Override
        public void addListener(IButtonPressedListener listener) {
            list.add(listener);
        }

        @Override
        public void removeListener(IButtonPressedListener listener) {
            list.remove(listener);
        }
    }

    public class GameButtonPressedEvent implements IListenerSupport<IButtonPressedListener> {

        private List<IButtonPressedListener> list = new ArrayList<>();
        private GameButtonPressedEvent() {}

        public void OnButtonPressed(final Buttons button) {
            if(!list.isEmpty()) {
                for(IButtonPressedListener tmp : list)
                    tmp.update(button);
            }
        }

        @Override
        public void addListener(IButtonPressedListener listener) {
            list.add(listener);
        }

        @Override
        public void removeListener(IButtonPressedListener listener) {
            list.remove(listener);
        }
    }
}
