package com.victoria.model.units;


public class Speed {
    public int speedX;
    public int speedY;
    public int offset;

    public Speed() {}

    public Speed(int offset) { this.offset = offset; }
}
