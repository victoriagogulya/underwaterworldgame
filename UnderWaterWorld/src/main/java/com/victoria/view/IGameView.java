package com.victoria.view;


import com.victoria.view.events.ButtonPressedEvent;
import com.victoria.view.events.KeyPressedEvent;

public interface IGameView extends IVisibility{

    void initialization();
    KeyPressedEvent getKeyPressedEvent();
    ButtonPressedEvent.GameButtonPressedEvent getButtonPressedEvent();
}
