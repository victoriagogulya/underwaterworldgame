package com.victoria.model.events;


import com.victoria.common.IListenerSupport;
import com.victoria.model.GameModelState;

import java.util.ArrayList;

public class GameModelChangedEvent implements IListenerSupport<IGameModelChangedListener> {

    private static final GameModelChangedEvent inst = new GameModelChangedEvent();
    private ArrayList<IGameModelChangedListener> listeners = new ArrayList<>();

    private GameModelChangedEvent(){}

    public static GameModelChangedEvent instance() {
        return inst;
    }

    public void OnGameChanged(GameModelState state) {
        if (!listeners.isEmpty()) {
            for (IGameModelChangedListener list : listeners)
                list.update(state);
        }
    }

    @Override
    public void addListener(IGameModelChangedListener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeListener(IGameModelChangedListener listener) {
        listeners.remove(listener);
    }
}
