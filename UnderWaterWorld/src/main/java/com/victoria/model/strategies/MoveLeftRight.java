package com.victoria.model.strategies;


import com.victoria.model.units.Speed;

public class MoveLeftRight extends MoveStrategy {

    private static final int MAX = 6;
    private int steps;
    private boolean directionLeft = true;
    @Override
    public void update(Speed speed) {
        if (directionLeft) {
            if (playerSpeed != null && playerSpeed.speedX > 0)
                speed.speedX = -(speed.offset + playerSpeed.speedX);
            else
                speed.speedX = -speed.offset;
        }
        else {
            if (playerSpeed != null && playerSpeed.speedX > 0)
                speed.speedX = speed.offset - playerSpeed.speedX;
            else
                speed.speedX = speed.offset;
        }
        steps++;
        if (steps == MAX) {
            steps = 0;
            directionLeft = !directionLeft;
        }
    }
}
