package com.victoria.model.units;


public class Coord {
    public int x;
    public int y;

    public Coord(){}

    public Coord(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
