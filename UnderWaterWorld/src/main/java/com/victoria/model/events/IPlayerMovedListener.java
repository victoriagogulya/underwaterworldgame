package com.victoria.model.events;

import com.victoria.model.units.Speed;


@FunctionalInterface
public interface IPlayerMovedListener {

    void update(Speed playerSpeed);
}
