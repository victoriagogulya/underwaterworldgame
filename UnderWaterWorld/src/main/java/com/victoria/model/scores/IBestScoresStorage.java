package com.victoria.model.scores;


import java.io.IOException;

public interface IBestScoresStorage {

    void save(IBestScores bestScores) throws IOException;
    IBestScores load() throws IOException, ClassNotFoundException;
}
