package com.victoria.controller;


import com.victoria.view.IMainView;


public class MainController implements IController {

    private final IMainView view;
    private final GameController gameController;

    public MainController(IMainView view, GameController gameController) {
        this.view = view;
        this.gameController = gameController;

        view.getButtonPressedEvent().addListener(button ->  {
                switch (button) {
                    case Play:
                        this.gameController.initialization();
                        view.visibility(false);
                        this.gameController.run();
                        break;
                }
        });
        gameController.getWorkIsDoneEvent().addListener( () -> {
            switch (gameController.getResultOfWork()) {
                case Exit:
                case GameOver:
                    gameController.stop();
                    break;
            }
            view.visibility(true);
        });
    }

    @Override
    public void run() {

        view.visibility(true);
    }

    @Override
    public void stop() { }
}
